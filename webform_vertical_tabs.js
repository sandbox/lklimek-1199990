/**
 * @file
 * Javascript that sets summary for webform vertical tabs.
 * Summaries are stored in settings.webformVerticalTabs in the form: key=>value 
 * where key is a name of fieldset and value is a summary (description) of fieldset.
 */

(function($) {

  Drupal.behaviors.webformVerticalTabs = {
    attach : function(context, settings) {

      var error_focused = false;
      $.each(settings.webformVerticalTabs, function(key, val) {
        if (key) {
          // find fieldsets with errors
          if (error_focused == false && $('fieldset.webform_vt_class_' + key, context, val).find('.error').length > 0) {
            // set fieldset as focused
            $('fieldset.webform_vt_class_' + key).data('verticalTab').focus();
            error_focused = true;
          }
          $('fieldset.webform_vt_class_' + key, context, val).drupalSetSummary(function(context) {
            return Drupal.t(val);
          });
        }
      });
    }
  };
})(jQuery);
